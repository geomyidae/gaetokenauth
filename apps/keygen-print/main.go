package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
)

func main() {
	key, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	fmt.Printf("Private key: (D) %X\n", key.D)
	fmt.Printf("Public  key: (X) %X\n", key.X)
	fmt.Printf("Public  key: (Y) %X\n", key.Y)
}
