package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/json"
	"flag"
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	uuidLib "github.com/hashicorp/go-uuid"
	"log"
	"math/big"
	"os"
	"time"
)

type Key struct {
	D string `json:"d"`
	X string `json:"x"`
	Y string `json:"y"`
}

func main() {
	var audience = flag.String("audience", "AUD", "Audience")
	var subject = flag.String("subject", "SUB", "Subject")
	var issuer = flag.String("issuer", "ISS", "Issuer")
	var uuid = flag.String("uuid", "", "UUID")
	var expiresAt = flag.String("expires-at", "", "Expires At (YYYY-MM-DD HH:MM)")
	var notBefore = flag.String("not-before", "", "Not before (YYYY-MM-DD HH:MM)")
	var keyFile = flag.String("key", "key.json", "Private key")
	flag.Parse()

	f, err := os.Open(*keyFile)
	if err != nil {
		log.Fatalf("Error opening %s: %v", *keyFile, err)
	}
	defer f.Close()

	var k Key
	dec := json.NewDecoder(f)
	dec.Decode(&k)

	keyD := new(big.Int)
	keyX := new(big.Int)
	keyY := new(big.Int)

	keyD.SetString(k.D, 16)
	keyX.SetString(k.X, 16)
	keyY.SetString(k.Y, 16)

	if *uuid == "" {
		*uuid, _ = uuidLib.GenerateUUID()
	}
	fmt.Printf("UUID: %s\n", *uuid)

	claims := &jwt.StandardClaims{
		Audience: *audience,
		Subject:  *subject,
		Issuer:   *issuer,
		Id:       *uuid,
		IssuedAt: time.Now().Unix(),
	}

	if *expiresAt != "" {
		t, err := time.Parse("2006-01-02 15:04", *expiresAt)
		if err != nil {
			log.Fatal(err)
		}
		claims.ExpiresAt = t.Unix()
	}

	if *notBefore != "" {
		t, err := time.Parse("2006-01-02 15:04", *notBefore)
		if err != nil {
			log.Fatal(err)
		}
		claims.NotBefore = t.Unix()
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)

	publicKey := ecdsa.PublicKey{
		Curve: elliptic.P256(),
		X:     keyX,
		Y:     keyY,
	}

	privateKey := ecdsa.PrivateKey{D: keyD, PublicKey: publicKey}
	ss, _ := token.SignedString(&privateKey)
	fmt.Println("----- BEGIN JWT SIGNED TOKEN -----")
	fmt.Println(ss)
	fmt.Println("----- END JWT SIGNED TOKEN -----")

}
