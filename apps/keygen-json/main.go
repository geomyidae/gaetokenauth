package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

type Key struct {
	D string `json:"d"`
	X string `json:"x"`
	Y string `json:"y"`
}

func main() {
	var out = flag.String("out", "key.json", "JSON key filename")
	flag.Parse()

	key, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)

	jKey := Key{
		D: fmt.Sprintf("%X", key.D),
		X: fmt.Sprintf("%X", key.X),
		Y: fmt.Sprintf("%X", key.Y),
	}

	f, err := os.Create(*out)
	defer f.Close()

	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}

	enc := json.NewEncoder(f)
	enc.SetIndent("", "  ")

	enc.Encode(&jKey)
}
