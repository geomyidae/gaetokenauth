package gaetokenauth

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/hashicorp/go-uuid"
	"github.com/stretchr/testify/assert"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/aetest"
	"google.golang.org/appengine/datastore"
	"strings"
	"testing"
)

var publicKeyX, publicKeyY string
var validUUID, invalidUUID string
var validToken, invalidToken, incompleteToken string

type token struct {
	Present bool
}

func insertToken(ctx context.Context, uuid string) error {
	t := token{
		Present: true,
	}
	key := datastore.NewKey(ctx, "ValidTokens", uuid, 0, nil)
	_, err := datastore.Put(ctx, key, &t)
	return err
}

func TestBasic(t *testing.T) {
	publicKey := NewPublicKey(publicKeyX, publicKeyY)
	if strings.ToUpper(publicKey.X.Text(16)) != publicKeyX {
		t.Errorf("invalid X (%v != %v)", strings.ToUpper(publicKey.X.Text(16)), publicKeyX)
	}
	if strings.ToUpper(publicKey.Y.Text(16)) != publicKeyY {
		t.Errorf("invalid Y (%v != %v)", strings.ToUpper(publicKey.Y.Text(16)), publicKeyY)
	}
}

func TestGooodAuth(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), validUUID)
	req.Header.Add("Authorization", "Bearer "+validToken)

	err = ta.CheckAuth(req)
	assert.NoError(t, err)

	// check cache
	err = ta.CheckAuth(req)
	assert.NoError(t, err)
}

func TestBadAuth(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), invalidUUID)
	req.Header.Add("Authorization", "Bearer "+validToken)

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "datastore: no such entity")
}

func TestNoAuth(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "Missing Authorization header")
}

func TestBadHeader(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), validUUID)
	req.Header.Add("Authorization", "Foobar "+validToken)

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "Bad Authorization header")
}

func TestInvalidToken(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), validUUID)
	req.Header.Add("Authorization", "Bearer "+invalidToken)

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "datastore: no such entity")
}

func TestIncompleteToken(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), validUUID)
	req.Header.Add("Authorization", "Bearer "+incompleteToken)

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "Token does not contain a UUID")
}

func TestBadToken(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), validUUID)
	req.Header.Add("Authorization", "Bearer FooBar")

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "token contains an invalid number of segments")
}

func TestWrongSignature(t *testing.T) {
	var ta = TokenAuth{
		PublicKey:      NewPublicKey(publicKeyX, publicKeyY),
		DatastoreKind:  "ValidTokens",
		MemcachePrefix: "valid-token-",
	}

	inst, err := aetest.NewInstance(nil)
	assert.NoError(t, err)
	defer inst.Close()

	req, err := inst.NewRequest("GET", "/", nil)
	assert.NoError(t, err)

	insertToken(appengine.NewContext(req), validUUID)

	badPrivateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	badSignedToken, err := jwt.NewWithClaims(jwt.SigningMethodES256, &jwt.StandardClaims{
		Audience: "basic",
		Subject:  "Jacques",
		Issuer:   "BlueMasters",
		Id:       validUUID,
	}).SignedString(badPrivateKey)

	if err != nil {
		panic(err)
	}
	req.Header.Add("Authorization", "Bearer "+badSignedToken)

	err = ta.CheckAuth(req)
	assert.Error(t, err)
	assert.EqualError(t, err, "crypto/ecdsa: verification error")
}

func init() {
	var err error

	validUUID, err = uuid.GenerateUUID()
	if err != nil {
		panic(err)
	}

	invalidUUID, err = uuid.GenerateUUID()
	if err != nil {
		panic(err)
	}

	goodPrivateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)

	publicKeyX = fmt.Sprintf("%X", goodPrivateKey.X)
	publicKeyY = fmt.Sprintf("%X", goodPrivateKey.Y)

	validToken, err = jwt.NewWithClaims(jwt.SigningMethodES256, &jwt.StandardClaims{
		Audience: "basic",
		Subject:  "Jacques",
		Issuer:   "BlueMasters",
		Id:       validUUID,
	}).SignedString(goodPrivateKey)

	if err != nil {
		panic(err)
	}

	invalidToken, err = jwt.NewWithClaims(jwt.SigningMethodES256, &jwt.StandardClaims{
		Audience: "basic",
		Subject:  "Jacques",
		Issuer:   "BlueMasters",
		Id:       invalidUUID,
	}).SignedString(goodPrivateKey)

	if err != nil {
		panic(err)
	}

	incompleteToken, err = jwt.NewWithClaims(jwt.SigningMethodES256, &jwt.StandardClaims{
		Audience: "basic",
		Subject:  "Jacques",
		Issuer:   "BlueMasters",
	}).SignedString(goodPrivateKey)

	if err != nil {
		panic(err)
	}

}
